<?php
/**
 * Created by bart
 */
include('components/head.html.php');
?>

<div class="row">
    <div class="col-xs-12">
        <div class="jumbotron text-center">
            <h1><?php echo $message ?></h1>
        </div>
    </div>
</div>

<?php
include('components/foot.html.php');
?>
