<?php
include('include/autoloader.php');
use Model\Form\LoginForm;
use Model\User\Authenticator;

session_start();

$loginMessage = "";

$loginForm = new LoginForm();
if ($loginForm->validatePostData($_POST)) {
    $email = $loginForm->getEmail();
    $password = $loginForm->getPassword();
    $authenticator = new Authenticator();
    if ($user = $authenticator->authenticate($email,$password)) {
        $_SESSION["leerkracht"] = $user;
        header('location: vakken.php');
    } else {
        $loginMessage = "Incorrect email and password combination";
    }
}
include('View/login.html.php');