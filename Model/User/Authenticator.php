<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 12.10.17
 * Time: 20:18
 */

namespace Model\User;

use Entity\Repository\LeerkrachtRepository;


class Authenticator {

    public function authenticate($email, $password) {
        $passwordEncoder = new PasswordEncoder();
        $encodedPassword = $passwordEncoder->encode($password);
        $leerkrachtRepository = new LeerkrachtRepository();
        $user = $leerkrachtRepository->findBy(array(LeerkrachtRepository::COLUMN_NAAM => "'".$email."'"));
        if ($user) {
           // return $user->getPassword() == $encodedPassword ? $user : null;
           return true;
        }
        return null;
    }
} 