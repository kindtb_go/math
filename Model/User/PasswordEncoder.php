<?php


namespace Model\User;


class PasswordEncoder {

    public function encode($password) {
        return $this->md5Encoder($password);
    }

    private function noEncoder($password) {
        return $password;
    }

    private function md5Encoder($password) {
        return md5($password);
    }

    private function hashEncoder($password) {
        return password_hash($password, PASSWORD_DEFAULT);
    }
} 