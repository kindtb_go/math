<?php
/**
 * Created by Bart
 */

namespace Model\Demo;

class Demo {

    private $message = 'This is the default message';

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

} 