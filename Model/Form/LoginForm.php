<?php
namespace Model\Form;

class LoginForm {
    const FORM_EMAIL = "email";
    const FORM_PASSWORD = "password";
    const FORM_SUBMIT = "submit";

    private $post;

    public function validatePostData($post) {
        $this->post = $post;
        return
            isset($post[self::FORM_EMAIL]) &&
            isset($post[self::FORM_PASSWORD]);
    }
    public function getEmail() {
        return $this->post[self::FORM_EMAIL];
    }
    public function getPassword() {
        return $this->post[self::FORM_PASSWORD];
    }
} 