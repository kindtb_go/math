-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 21, 2017 at 05:47 PM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `math`
--

-- --------------------------------------------------------

--
-- Table structure for table `Tbl_DoelstellingPerVraag`
--

CREATE TABLE IF NOT EXISTS `Tbl_DoelstellingPerVraag` (
  `IDDoelstellingPerVraag` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Vraag` int(11) NOT NULL,
  `Doelstelling` int(11) NOT NULL,
  PRIMARY KEY (`IDDoelstellingPerVraag`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Tbl_DoelstellingPerVraag`
--

INSERT INTO `Tbl_DoelstellingPerVraag` (`IDDoelstellingPerVraag`, `Vraag`, `Doelstelling`) VALUES
  (1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Tbl_GesteldeVragen`
--

CREATE TABLE IF NOT EXISTS `Tbl_GesteldeVragen` (
  `IDGesteldeVraag` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `GevraagdOpWelkeToets` int(11) NOT NULL,
  `Vraag` int(11) NOT NULL,
  `AantalPuntenVraag` int(11) NOT NULL,
  PRIMARY KEY (`IDGesteldeVraag`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Tbl_GesteldeVragen`
--

INSERT INTO `Tbl_GesteldeVragen` (`IDGesteldeVraag`, `GevraagdOpWelkeToets`, `Vraag`, `AantalPuntenVraag`) VALUES
  (1, 1, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `Tbl_klassen`
--

CREATE TABLE IF NOT EXISTS `Tbl_klassen` (
  `IDKlas` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Klas` varchar(32) NOT NULL,
  PRIMARY KEY (`IDKlas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Tbl_klassen`
--

INSERT INTO `Tbl_klassen` (`IDKlas`, `Klas`) VALUES
  (1, '6INFO'),
  (2, '5INFO');

-- --------------------------------------------------------

--
-- Table structure for table `Tbl_Layout`
--

CREATE TABLE IF NOT EXISTS `Tbl_Layout` (
  `IDLayout` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Header` text NOT NULL,
  PRIMARY KEY (`IDLayout`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Tbl_Layout`
--

INSERT INTO `Tbl_Layout` (`IDLayout`, `Header`) VALUES
  (1, '<p>uw naam: </p>');

-- --------------------------------------------------------

--
-- Table structure for table `Tbl_Leerkracht`
--

CREATE TABLE IF NOT EXISTS `Tbl_Leerkracht` (
  `Stamboeknummer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Naam` varchar(32) NOT NULL,
  `Voornaam` varchar(32) NOT NULL,
  PRIMARY KEY (`Stamboeknummer`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Tbl_Leerkracht`
--

INSERT INTO `Tbl_Leerkracht` (`Stamboeknummer`, `Naam`, `Voornaam`) VALUES
  (1, 'Vande Riviere', 'Katrijn'),
  (2, 'Depuydt', 'Liselotte');

-- --------------------------------------------------------

--
-- Table structure for table `Tbl_Leerplan`
--

CREATE TABLE IF NOT EXISTS `Tbl_Leerplan` (
  `IDLeerplan` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Leerplannummer` varchar(16) NOT NULL,
  `Vak` int(11) NOT NULL,
  PRIMARY KEY (`IDLeerplan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Tbl_Leerplan`
--

INSERT INTO `Tbl_Leerplan` (`IDLeerplan`, `Leerplannummer`, `Vak`) VALUES
  (1, '2005-069', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Tbl_LeerplanDoelstellingen`
--

CREATE TABLE IF NOT EXISTS `Tbl_LeerplanDoelstellingen` (
  `IDLeerplanDoestelling` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Leerplan` int(11) NOT NULL,
  `Doelstelling` text NOT NULL,
  PRIMARY KEY (`IDLeerplanDoestelling`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Tbl_LeerplanDoelstellingen`
--

INSERT INTO `Tbl_LeerplanDoelstellingen` (`IDLeerplanDoestelling`, `Leerplan`, `Doelstelling`) VALUES
  (1, 1, 'Leer optellen');

-- --------------------------------------------------------

--
-- Table structure for table `Tbl_Oplossingen`
--

CREATE TABLE IF NOT EXISTS `Tbl_Oplossingen` (
  `IDOplossing` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Vraag` int(11) NOT NULL,
  `Oplossing` text NOT NULL,
  PRIMARY KEY (`IDOplossing`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Tbl_Oplossingen`
--

INSERT INTO `Tbl_Oplossingen` (`IDOplossing`, `Vraag`, `Oplossing`) VALUES
  (1, 1, '2');

-- --------------------------------------------------------

--
-- Table structure for table `Tbl_Schooljaar`
--

CREATE TABLE IF NOT EXISTS `Tbl_Schooljaar` (
  `IDSchooljaar` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Jaar` int(11) NOT NULL,
  PRIMARY KEY (`IDSchooljaar`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Tbl_Schooljaar`
--

INSERT INTO `Tbl_Schooljaar` (`IDSchooljaar`, `Jaar`) VALUES
  (1, 2017);

-- --------------------------------------------------------

--
-- Table structure for table `Tbl_SoortenEvaluaties`
--

CREATE TABLE IF NOT EXISTS `Tbl_SoortenEvaluaties` (
  `IDEvaluaties` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SoortEvaluatie` text NOT NULL,
  PRIMARY KEY (`IDEvaluaties`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Tbl_SoortenEvaluaties`
--

INSERT INTO `Tbl_SoortenEvaluaties` (`IDEvaluaties`, `SoortEvaluatie`) VALUES
  (1, 'Herhalingstoets');

-- --------------------------------------------------------

--
-- Table structure for table `Tbl_Toetsen`
--

CREATE TABLE IF NOT EXISTS `Tbl_Toetsen` (
  `IDToetsen` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Datum` varchar(16) NOT NULL,
  `Leerkracht` int(11) NOT NULL,
  `Vak` int(11) NOT NULL,
  `Omschrijving` text NOT NULL,
  `Titel` text NOT NULL,
  `SoortEvaluatie` int(11) NOT NULL,
  PRIMARY KEY (`IDToetsen`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Tbl_Toetsen`
--

INSERT INTO `Tbl_Toetsen` (`IDToetsen`, `Datum`, `Leerkracht`, `Vak`, `Omschrijving`, `Titel`, `SoortEvaluatie`) VALUES
  (1, '16/11/2017', 1, 1, 'toets optellen', 'Toets optellen', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Tbl_Vak`
--

CREATE TABLE IF NOT EXISTS `Tbl_Vak` (
  `IDVak` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Naam` varchar(32) NOT NULL,
  `Klas` int(11) NOT NULL,
  `Leerplan` int(11) NOT NULL,
  `Leerkracht` int(11) NOT NULL,
  `Schooljaar` int(11) NOT NULL,
  PRIMARY KEY (`IDVak`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Tbl_Vak`
--

INSERT INTO `Tbl_Vak` (`IDVak`, `Naam`, `Klas`, `Leerplan`, `Leerkracht`, `Schooljaar`) VALUES
  (1, 'Wiskunde', 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Tbl_Vragen`
--

CREATE TABLE IF NOT EXISTS `Tbl_Vragen` (
  `IDVraag` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Vraag` text NOT NULL,
  PRIMARY KEY (`IDVraag`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Tbl_Vragen`
--

INSERT INTO `Tbl_Vragen` (`IDVraag`, `Vraag`) VALUES
  (1, 'hoeveel is 1 + 1');
