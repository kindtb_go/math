<?php
namespace Entity;

use Entity\Repository\ToetsRepository;
use Entity\Repository\VraagRepository;

class GesteldeVraag {

    /** @var int */
    private $id;

    /** @var string */
    private $gevraagdOpWelkeToets;

    /** @var string */
    private $vraag;

    /** @var string */
    private $aantalPuntenVraag;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getGevraagdOpWelkeToets() {
        $toetsRepository = new ToetsRepository();
        $gevraagdOpWelkeToets = $toetsRepository ->find($this->gevraagdOpWelkeToets);
        return $gevraagdOpWelkeToets;
    }

    /**
     * @param string $gevraagdOpWelkeToets
     */
    public function setGevraagdOpWelkeToets($gevraagdOpWelkeToets) {
        $this->gevraagdOpWelkeToets = $gevraagdOpWelkeToets;
    }

    /**
     * @return string
     */
    public function getVraag()
    {
        $vraagRepository = new VraagRepository();
        $vraag = $vraagRepository ->find($this->vraag);
        return $vraag;
    }

    /**
     * @param string $vraag
     */
    public function setVraag($vraag)
    {
        $this->vraag = $vraag;
    }

    /**
     * @return string
     */
    public function getAantalPuntenVraag()
    {
        return $this->aantalPuntenVraag;
    }

    /**
     * @param string $aantalPuntenVraag
     */
    public function setAantalPuntenVraag($aantalPuntenVraag)
    {
        $this->aantalPuntenVraag = $aantalPuntenVraag;
    }

}