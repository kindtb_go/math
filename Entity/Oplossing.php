<?php

namespace Entity;

use Entity\Repository\VraagRepository;

class Oplossing{

        private $id;
        private $vraag;
        private $oplossing;
    
        public function getId()
        {
            return $this->id;
        }
    
        public function setId($id)
        {
            $this->id = $id;
        }
    
        public function getVraag()
        {
            $vraagRepository = new VraagRepository();
            $vraag = $vraagRepository->find($this->vraag);
            return $vraag ;
        }
    
        public function setVraag($vraag)
        {
            $this->vraag = $vraag;
        }
        
         public function getOplossing()
        {
            return $this->oplossing ;
        }
    
        public function setOplossing($oplossing)
        {
            $this->oplossing = $oplossing;
        }

    }
