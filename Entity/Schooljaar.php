<?php

namespace Entity;

use Entity\Repository\VakRepository;

class Schooljaar
{
    private $id;
    private $jaar;

    private $vakken;
    
    public function getId()
    {
        return $this->id;
    }
    public function setId($id)
    {
        $this->id = $id;
    }
    
    public function getJaar()
    {
        return $this->jaar;
    }
    
    public function setJaar($jaar)
    {
        $this->jaar = $jaar;
    }

    public function getVakken() {
        if (is_null($this->vakken)) {
            $vakRepository = new VakRepository();
            $this->vakken = $vakRepository->findBy(array(
                VakRepository::COLUMN_SCHOOLJAAR => $this->id
            ));
        }

        return $this->vakken;
    }
}