<?php
namespace Entity;

use Entity\Repository\LeerplanDoelstellingRepository;
use Entity\Repository\VraagRepository;

class DoelstellingPerVraag
{
    private $id;

    private $doelstelling;

    private $vraag;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getDoelstelling()
    {
        $doelstellingRepository = new LeerplanDoelstellingRepository();
        $doelstelling = $doelstellingRepository->find($this->doelstelling);
        return $doelstelling;
    }

    public function setDoelstelling($doelstelling)
    {
        $this->doelstelling = $doelstelling;
    }

    public function getVraag()
    {
        $vraagRepository = new VraagRepository();
        $vraag = $vraagRepository->find($this->vraag);
        return $vraag;
    }

    public function setVraag($vraag)
    {
        $this->vraag = $vraag;
    }
}