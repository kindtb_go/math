<?php
namespace Entity;

use Entity\Repository\LeerplanDoelstellingRepository;
use Entity\Repository\VakRepository;

class Leerplan {

    /** @var int */
    private $id;

    /** @var string */
    private $leerplanNummer;

    /** @var array */
    private $doelstellingen = null;

    /** @var array */
    private $vakken = null;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getLeerplanNummer() {
        return $this->leerplanNummer;
    }
    
        /**
     * @param string $leerplanNummer
     */
    public function setLeerplanNummer($leerplanNummer) {
        $this->leerplanNummer = $leerplanNummer;
    }


    public function getVakken() {
        if (is_null($this->vakken)) {
            $vakRepository = new VakRepository();
            $this->vakken = $vakRepository->findBy(array(
                VakRepository::COLUMN_LEERPLAN => $this->id
            ));
        }
        return $this->vakken;
    }
    
    public function getDoelstellingen() {
        if (is_null($this->doelstellingen)) {
            $doelstellingRepository = new LeerplanDoelstellingRepository();
            $this->doelstellingen = $doelstellingRepository->findBy(array(
                LeerplanDoelstellingRepository::COLUMN_LEERPLAN => $this->id
            ));
        }
        return $this->doelstellingen;
    }

} 