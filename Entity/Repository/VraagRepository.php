<?php
namespace Entity\Repository;

use Entity\Vraag;

class VraagRepository {
    
    const TABLE_NAME = "Tbl_Vragen";

    const COLUMN_ID = "IDVraag";
    const COLUMN_VRAAG = "Vraag";
    
    private $dbConn;

    public function __construct()
    {
        $this->dbConn = mysqli_connect(Parameters::DB_HOST, Parameters::DB_USERNAME, Parameters::DB_PASSWORD, Parameters::DB_NAME);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME;
        $result = $this->dbConn->query($sql);

        $vragen = array();
        while ($row = $result->fetch_assoc()) {
            $vragen[] = $this->createObject($row);
        }
        return $vragen;
    }
    
    public function find($id)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE ".self::COLUMN_ID."=".$id;
        $result = $this->dbConn->query($sql);

        if ($row = $result->fetch_assoc()) {
            return $this->createObject($row);
        }
        return null;
    }
    
    private function createObject($row)
    {
        $vraag = new Vraag();
        $vraag->setId($row[self::COLUMN_ID]);
        $vraag->setVraag($row[self::COLUMN_VRAAG]);

        return $vraag;
    }
}