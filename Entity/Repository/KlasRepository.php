<?php
namespace Entity\Repository;

use Entity\Klas;

class KlasRepository {
    
    const TABLE_NAME = "Tbl_klassen";

    const COLUMN_ID = "IDKlas";
    const COLUMN_KLAS = "Klas";
    
    private $dbConn;

    public function __construct()
    {
        $this->dbConn = mysqli_connect(Parameters::DB_HOST, Parameters::DB_USERNAME, Parameters::DB_PASSWORD, Parameters::DB_NAME);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME;
        $result = $this->dbConn->query($sql);

        $klassen = array();
        while ($row = $result->fetch_assoc()) {
            $klassen[] = $this->createObject($row);
        }
        return $klassen;
    }
    
    public function find($id)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE ".self::COLUMN_ID."=".$id;
        $result = $this->dbConn->query($sql);

        if ($row = $result->fetch_assoc()) {
            return $this->createObject($row);
        }
        return null;
    }

    public function findBy($criteria) {
        $where = array();
        foreach ($criteria as $field => $value) {
            $where[] = $field . "=" . $value;
        }
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE " . implode(" AND ",$where);
        $result = $this->dbConn->query($sql);

        $toetsen = array();
        while ($row = $result->fetch_assoc()) {
            $toetsen[] = $this->createObject($row);
        }
        return $toetsen;
    }

    private function createObject($row)
    {
        $klas = new klas();
        $klas->setId($row[self::COLUMN_ID]);
        $klas->setklas($row[self::COLUMN_KLAS]);

        return $klas;
    }
}