<?php

namespace Entity\Repository;

use Entity\Layout;


class LayoutRepository {
    
    const TABLE_NAME = "Tbl_Layout";


    const COLUMN_ID = "IDLayout";
    const COLUMN_HEADER = "Header";
    
    private $dbConn;


    public function __construct()
    {
        $this->dbConn = mysqli_connect(Parameters::DB_HOST, Parameters::DB_USERNAME, Parameters::DB_PASSWORD, Parameters::DB_NAME);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME;
        $result = $this->dbConn->query($sql);

        $headers = array();
        while ($row = $result->fetch_assoc()) {
            $headers[] = $this->createObject($row);
        }
        return $headers;

    }
    
    public function find($id)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE ".self::COLUMN_ID."=".$id;

        $result = $this->dbConn->query($sql);

        if ($row = $result->fetch_assoc()) {
            return $this->createObject($row);
        }
        return null;
    }
    
    private function createObject($row)
    {
        $header = new Layout();
        $header->setId($row[self::COLUMN_ID]);
        $header->setHeader($row[self::COLUMN_HEADER]);

        return $header;
    }
    
}

?>

