<?php
namespace Entity\Repository;

use Entity\Leerplan;

class LeerplanRepository {
    
    const TABLE_NAME = "Tbl_Leerplan";

    const COLUMN_ID = "IDLeerplan";
    const COLUMN_LEERPLANNUMMER = "Leerplannummer";
    
    private $dbConn;

    public function __construct()
    {
        $this->dbConn = mysqli_connect(Parameters::DB_HOST, Parameters::DB_USERNAME, Parameters::DB_PASSWORD, Parameters::DB_NAME);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME;
        $result = $this->dbConn->query($sql);

        $leerplan = array();
        while ($row = $result->fetch_assoc()) {
            $leerplan[] = $this->createObject($row);
        }
        return $leerplan;
    }
    
    public function find($id)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE ".self::COLUMN_ID."=".$id;
        $result = $this->dbConn->query($sql);

        if ($row = $result->fetch_assoc()) {
            return $this->createObject($row);
        }
        return null;
    }
    
    private function createObject($row)
    {
        $leerplan = new Leerplan();
        $leerplan->setId($row[self::COLUMN_ID]);
        $leerplan->setLeerplanNummer($row[self::COLUMN_LEERPLANNUMMER]);
        return $leerplan;
    }
}