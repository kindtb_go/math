<?php
namespace Entity\Repository;

use Entity\GesteldeVraag;

class GesteldeVraagRepository {
    
    const TABLE_NAME = "Tbl_GesteldeVragen";

    const COLUMN_IDGESTELDEVRAAG = "IDGesteldeVraag";
    const COLUMN_GEVRAAGDOPWELKETOETS = "GevraagdOpWelkeToets";
    const COLUMN_VRAAG = "Vraag";
    const COLUMN_AANTALPUNTENVRAAG = "AantalPuntenVraag";
    
    private $dbConn;

    public function __construct()
    {
        $this->dbConn = mysqli_connect(Parameters::DB_HOST, Parameters::DB_USERNAME, Parameters::DB_PASSWORD, Parameters::DB_NAME);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME;
        $result = $this->dbConn->query($sql);

        $gesteldevragen = array();
        while ($row = $result->fetch_assoc()) {
            $gesteldevragen[] = $this->createObject($row);
        }
        return $gesteldevragen;
    }
    
    public function find($id)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE ".self::COLUMN_IDGESTELDEVRAAG."=".$id;
        $result = $this->dbConn->query($sql);

        if ($row = $result->fetch_assoc()) {
            return $this->createObject($row);
        }
        return null;
    }
    
     public function findBy($criteria) {
       $where = array();
       foreach ($criteria as $field => $value) {
           $where[] = $field . "=" . $value;
       }
       $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE " . implode(" AND ",$where);
       $result = $this->dbConn->query($sql);

       $vakken = array();
       while ($row = $result->fetch_assoc()) {
           $gesteldevragen[] = $this->createObject($row);
       }
       return $gesteldevragen;
    }

    
    private function createObject($row)
    {
        $gesteldevragen = new GesteldeVraag();
        $gesteldevragen->setId($row[self::COLUMN_IDGESTELDEVRAAG]);
        $gesteldevragen->setGevraagdOpWelkeToets($row[self::COLUMN_GEVRAAGDOPWELKETOETS]);
        $gesteldevragen->setVraag($row[self::COLUMN_VRAAG]);
        $gesteldevragen->setAantalPuntenVraag($row[self::COLUMN_AANTALPUNTENVRAAG]);

        return $gesteldevragen;
    }
    
    public function findBy($criteria) {
       $where = array();
       foreach ($criteria as $field => $value) {
           $where[] = $field . "=" . $value;
       }
       $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE " . implode(" AND ",$where);
       $result = $this->dbConn->query($sql);

       $gesteldeVragen = array();
       while ($row = $result->fetch_assoc()) {
           $gesteldeVragen[] = $this->createObject($row);
       }
       return $gesteldeVragen;
    }
}