<?php
namespace Entity\Repository;

use Entity\LeerplanDoelstelling;

class LeerplanDoelstellingRepository {
    
    const TABLE_NAME = "Tbl_LeerplanDoelstellingen";

    const COLUMN_ID = "IDLeerplanDoestelling";
    const COLUMN_LEERPLAN = "Leerplan";
    const COLUMN_DOELSTELLING = "Doelstelling";
    
    private $dbConn;

    public function __construct()
    {
        $this->dbConn = mysqli_connect(Parameters::DB_HOST, Parameters::DB_USERNAME, Parameters::DB_PASSWORD, Parameters::DB_NAME);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME;
        $result = $this->dbConn->query($sql);

        $Leerplannen = array();
        while ($row = $result->fetch_assoc()) {
            $Leerplannen[] = $this->createObject($row);
        }
        return $Leerplannen;
    }
    
    public function find($id)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE ".self::COLUMN_ID."=".$id;
        $result = $this->dbConn->query($sql);

        if ($row = $result->fetch_assoc()) {
            return $this->createObject($row);
        }
        return null;
    }

    public function findBy($criteria) {
        $where = array();
        foreach ($criteria as $field => $value) {
            $where[] = $field . "=" . $value;
        }
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE " . implode(" AND ",$where);
        $result = $this->dbConn->query($sql);

        $toetsen = array();
        while ($row = $result->fetch_assoc()) {
            $toetsen[] = $this->createObject($row);
        }
        return $toetsen;
    }

    private function createObject($row)
    {
        $leerplanDoelstelling = new LeerplanDoelstelling();
        $leerplanDoelstelling->setId($row[self::COLUMN_ID]);
        $leerplanDoelstelling->setLeerplan($row[self::COLUMN_LEERPLAN]);
        $leerplanDoelstelling->setDoelstelling($row[self::COLUMN_DOELSTELLING]);

        return $leerplanDoelstelling;
    }
}