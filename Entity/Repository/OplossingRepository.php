<?php

namespace Entity\Repository;

use Entity\Oplossing;

class OplossingRepository {
    
    const TABLE_NAME = "Tbl_Oplossingen";

    const COLUMN_ID = "IDOplossing";
    const COLUMN_VRAAG = "Vraag";
    const COLUMN_OPLOSSING = "Oplossing";
    
    private $dbConn;

    public function __construct()
    {
        $this->dbConn = mysqli_connect(Parameters::DB_HOST, Parameters::DB_USERNAME, Parameters::DB_PASSWORD, Parameters::DB_NAME);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME;
        $result = $this->dbConn->query($sql);

        $oplossingen = array();
        while ($row = $result->fetch_assoc()) {
            $oplossingen[] = $this->createObject($row);
        }
        return $oplossingen;
    }
    
    public function find($id)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE ".self::COLUMN_ID."=".$id;
        $result = $this->dbConn->query($sql);

        if ($row = $result->fetch_assoc()) {
            return $this->createObject($row);
        }
        return null;
    }
    
    private function createObject($row)
    {
        $oplossing = new Oplossing();
        $oplossing->setId($row[self::COLUMN_ID]);
        $oplossing->setvraag($row[self::COLUMN_VRAAG]);
        $oplossing->setOplossing($row[self::COLUMN_OPLOSSING]);

        return $oplossing;
    }
}