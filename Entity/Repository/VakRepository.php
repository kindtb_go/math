<?php

namespace Entity\Repository;

use Entity\Vak;

class VakRepository
{
    const TABLE_NAME = "Tbl_Vak";
    const COLUMN_ID = "IDVak";
    const COLUMN_NAAM = "Naam";
    const COLUMN_KLAS = "Klas";
    const COLUMN_LEERPLAN = "Leerplan";
    const COLUMN_LEERKRACHT = "Leerkracht";
    const COLUMN_SCHOOLJAAR = "Schooljaar";


    private $dbConn;

    public function __construct()
    {
        $this->dbConn = mysqli_connect(Parameters::DB_HOST, Parameters::DB_USERNAME, Parameters::DB_PASSWORD, Parameters::DB_NAME);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME;
        $result = $this->dbConn->query($sql);

        $vakken = array();
        while ($row = $result->fetch_assoc()) {
            $vakken[] = $this->createObject($row);
        }
        return $vakken;
    }

    public function find($id)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE ".self:: COLUMN_ID."=".$id;
        $result = $this->dbConn->query($sql);

        if ($row = $result->fetch_assoc()) {
            return $this->createObject($row);
        }
        return null;
    }

    public function findBy($criteria) {
        $where = array();
        foreach ($criteria as $field => $value) {
            $where[] = $field . "=" . $value;
        }
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE " . implode(" AND ",$where);
        $result = $this->dbConn->query($sql);

        $vakken = array();
        while ($row = $result->fetch_assoc()) {
            $vakken[] = $this->createObject($row);
        }
        return $vakken;
    }

    private function createObject($row)
    {
        $vak = new Vak();
        $vak->setId($row[self:: COLUMN_ID]);
        $vak->setNaam($row[self::COLUMN_NAAM]);
        $vak->setKlas($row[self::COLUMN_KLAS]);
        $vak->setLeerplan($row[self::COLUMN_LEERPLAN]);
        $vak->setLeerkracht($row[self::COLUMN_LEERKRACHT]);
        $vak->setSchooljaar($row[self::COLUMN_SCHOOLJAAR]);
        return $vak;
    }

    //public function findBy($criteria)

}


