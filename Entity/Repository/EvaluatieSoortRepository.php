<?php

namespace Entity\Repository;

use Entity\EvaluatieSoort;

class EvaluatieSoortRepository
{
    const TABLE_NAME = "Tbl_SoortenEvaluaties";

    const COLUMN_ID = "IDEvaluaties";
    const COLUMN_NAAM = "SoortEvaluatie";

    private $dbConn;

    public function __construct()
    {
        $this->dbConn = mysqli_connect(Parameters::DB_HOST, Parameters::DB_USERNAME, Parameters::DB_PASSWORD, Parameters::DB_NAME);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME;
        $result = $this->dbConn->query($sql);

        $users = array();
        while ($row = $result->fetch_assoc()) {
            $users[] = $this->createObject($row);
        }
        return $users;
    }

    public function find($id)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE ".self::COLUMN_ID."=".$id;
        $result = $this->dbConn->query($sql);

        if ($row = $result->fetch_assoc()) {
            return $this->createObject($row);
        }
        return null;
    }

    public function findBy($criteria) {
        $where = array();
        foreach ($criteria as $field => $value) {
            $where[] = $field . "=" . $value;
        }
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE " . implode(" AND ",$where);
        $result = $this->dbConn->query($sql);

        $objects = array();
        while ($row = $result->fetch_assoc()) {
            $objects[] = $this->createObject($row);
        }
        return $objects;
    }


    private function createObject($row)
    {
        $user = new EvaluatieSoort();
        $user->setId($row[self::COLUMN_ID]);
        $user->setNaam($row[self::COLUMN_NAAM]);
        
        return $user;
    }

}

?>