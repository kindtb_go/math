<?php
namespace Entity\Repository;

use Entity\Leerkracht;

class LeerkrachtRepository {
    
    const TABLE_NAME = "Tbl_Leerkracht";

    const COLUMN_ID = "Stamboeknummer";
    const COLUMN_NAAM = "Naam";
    const COLUMN_VOORNAAM = "Voornaam";
    
    private $dbConn;

    public function __construct()
    {
        $this->dbConn = mysqli_connect(Parameters::DB_HOST, Parameters::DB_USERNAME, Parameters::DB_PASSWORD, Parameters::DB_NAME);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME;
        $result = $this->dbConn->query($sql);

        $leerkrachten = array();
        while ($row = $result->fetch_assoc()) {
            $leerkrachten[] = $this->createObject($row);
        }
        return $leerkrachten;
    }
    
    public function find($stamboeknummer)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE ".self::COLUMN_ID."=".$stamboeknummer;
        $result = $this->dbConn->query($sql);

        if ($row = $result->fetch_assoc()) {
            return $this->createObject($row);
        }
        return null;
    }

    public function findBy($criteria) {
        $where = array();
        foreach ($criteria as $field => $value) {
            $where[] = $field . "=" . $value;
        }
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE " . implode(" AND ",$where);
        $result = $this->dbConn->query($sql);
        
        $toetsen = array();
        while ($row = $result->fetch_assoc()) {
            $toetsen[] = $this->createObject($row);
        }
        return $toetsen;
    }

    private function createObject($row)
    {
        $leerkrachten = new leerkracht();
        $leerkrachten->setId($row[self::COLUMN_ID]);
        $leerkrachten->setNaam($row[self::COLUMN_NAAM]);
        $leerkrachten->setVoornaam($row[self::COLUMN_VOORNAAM]);

        return $leerkrachten;
    }
}