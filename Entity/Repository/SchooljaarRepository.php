<?php

namespace Entity\Repository;

use Entity\Schooljaar;

class SchooljaarRepository
{
    const TABLE_NAME = "Tbl_Schooljaar";

    const COLUMN_ID = "IDSchooljaar";
    const COLUMN_YEAR = "Jaar";

    private $dbConn;

    public function __construct()
    {
        $this->dbConn = mysqli_connect(Parameters::DB_HOST, Parameters::DB_USERNAME, Parameters::DB_PASSWORD, Parameters::DB_NAME);
    }
    
    public function findAll()
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME;
        $result = $this->dbConn->query($sql);

        $schooljaren = array();
        while ($row = $result->fetch_assoc()) {
            $schooljaren[] = $this->createObject($row);
        }
        return $schooljaren;
    }

    public function find($id)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE ".self::COLUMN_ID."=".$id;
        $result = $this->dbConn->query($sql);

        if ($row = $result->fetch_assoc()) {
            return $this->createObject($row);
        }
        return null;
    }
    
    private function createObject($row)
    {
        $schooljaar = new Schooljaar();
        $schooljaar->setId($row[self::COLUMN_ID]);
        $schooljaar->setJaar($row[self::COLUMN_YEAR]);

        return $schooljaar;
    }
}
