<?php

namespace Entity\Repository;

use Entity\DoelstellingPerVraag;
use Entity\Toets;

class DoelstellingPerVraagRepository
{
    const TABLE_NAME = "Tbl_DoelstellingPerVraag";
    const COLUMN_ID = "IDDoelstellingPerVraag";
    const COLUMN_VRAAG = "Vraag";
    const COLUMN_DOELSTELLING = "Doelstelling";

    private $dbConn;

    public function __construct()
    {
        $this->dbConn = mysqli_connect(Parameters::DB_HOST, Parameters::DB_USERNAME, Parameters::DB_PASSWORD, Parameters::DB_NAME);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME;
        $result = $this->dbConn->query($sql);

        $toetsen = array();
        while ($row = $result->fetch_assoc()) {
            $toetsen[] = $this->createObject($row);
        }
        return $toetsen;
    }

    public function find($id)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE ".self:: COLUMN_ID."=".$id;
        $result = $this->dbConn->query($sql);

        if ($row = $result->fetch_assoc()) {
            return $this->createObject($row);
        }
        return null;
    }

    public function findBy($criteria) {
        $where = array();
        foreach ($criteria as $field => $value) {
            $where[] = $field . "=" . $value;
        }
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE " . implode(" AND ",$where);
        $result = $this->dbConn->query($sql);

        $objects = array();
        while ($row = $result->fetch_assoc()) {
            $objects[] = $this->createObject($row);
        }
        return $objects;
    }

    private function createObject($row)
    {
        $doelstellingPerVraag = new DoelstellingPerVraag();
        $doelstellingPerVraag->setId($row[self:: COLUMN_ID]);
        $doelstellingPerVraag->setVraag($row[self::COLUMN_VRAAG]);
        $doelstellingPerVraag->setDoelstelling($row[self::COLUMN_DOELSTELLING]);

        return $doelstellingPerVraag;
    }

}

