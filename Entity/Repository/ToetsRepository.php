<?php

namespace Entity\Repository;

use Entity\Toets;

class ToetsRepository
{
    const TABLE_NAME = "Tbl_Toetsen";
    const COLUMN_ID = "IDToetsen";
    const COLUMN_DATUM = "Datum";
    const COLUMN_LEERKRACHT = "Leerkracht";
    const COLUMN_VAK = "Vak";
    const COLUMN_OMSCHRIJVING = "Omschrijving";
    const COLUMN_TITEL = "Titel";
    const COLUMN_SOORT_EVALUATIE = "SoortEvaluatie";

    private $dbConn;

    public function __construct()
    {
        $this->dbConn = mysqli_connect(Parameters::DB_HOST, Parameters::DB_USERNAME, Parameters::DB_PASSWORD, Parameters::DB_NAME);
    }

    public function findAll()
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME;
        $result = $this->dbConn->query($sql);

        $toetsen = array();
        while ($row = $result->fetch_assoc()) {
            $toetsen[] = $this->createObject($row);
        }
        return $toetsen;
    }

    public function find($id)
    {
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE ".self:: COLUMN_ID."=".$id;
        $result = $this->dbConn->query($sql);

        if ($row = $result->fetch_assoc()) {
            return $this->createObject($row);
        }
        return null;
    }

    public function findBy($criteria) {
        $where = array();
        foreach ($criteria as $field => $value) {
            $where[] = $field . "=" . $value;
        }
        $sql = "SELECT * FROM ".self::TABLE_NAME." WHERE " . implode(" AND ",$where);
        $result = $this->dbConn->query($sql);

        $toetsen = array();
        while ($row = $result->fetch_assoc()) {
            $toetsen[] = $this->createObject($row);
        }
        return $toetsen;
    }

    private function createObject($row)
    {
        $toets = new Toets();
        $toets->setId($row[self:: COLUMN_ID]);
        $toets->setDatum($row[self::COLUMN_DATUM]);
        $toets->setLeerkracht($row[self::COLUMN_LEERKRACHT]);
        $toets->setVak($row[self::COLUMN_VAK]);
        $toets->setOmschrijving($row[self::COLUMN_OMSCHRIJVING]);
        $toets->setTitel($row[self::COLUMN_TITEL]);
        $toets->setSoortEvaluatie($row[self::COLUMN_SOORT_EVALUATIE]);

        return $toets;
    }

}

