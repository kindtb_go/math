<?php
namespace Entity;

use Entity\Repository\ToetsRepository;

class EvaluatieSoort {

    /** @var int */
    private $id;

    /** @var string */
    private $naam;

    /** @var array */
    private $toetsen = null;

    
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNaam() {
        return $this->naam;
    }

    /**
     * @param string $naam
     */
    public function setNaam($naam) {
        $this->naam = $naam;
    }

    public function getToetsen() {
        if (is_null($this->toetsen)) {
            $toetsRepository = new ToetsRepository();
            $this->toetsen = $toetsRepository->findBy(array(
                ToetsRepository::COLUMN_SOORT_EVALUATIE => $this->getId()
            ));
        }
        return $this->toetsen;
    }
   

} 

?>