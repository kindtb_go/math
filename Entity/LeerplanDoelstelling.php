<?php
namespace Entity;

use Entity\Repository\DoelstellingPerVraagRepository;
use Entity\Repository\LeerplanRepository;

class LeerplanDoelstelling {

    /** @var int */
    private $id;

    /** @var string */
    private $leerplan;

    /** @var string */
    private $doelstelling;

    /** @var array */
    private $vragen = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLeerplan() 
    {
        $leerplanRepository = new LeerplanRepository();
        $leerplan = $leerplanRepository->find($this->leerplan);
        return $leerplan;
    }

    /**
     * @param string $leerplan
     */
    public function setLeerplan($leerplan) {
        $this->leerplan = $leerplan;
    }

    /**
     * @return string
     */
    public function getDoelstelling()
    {
        return $this->doelstelling;
    }

    /**
     * @param string $doelstelling
     */
    public function setDoelstelling($doelstelling)
    {
        $this->doelstelling = $doelstelling;
    }
    
    private $id;
    private $vragen = null;
    
    public function getVragen(){
        if (is_null($this->vragen)) {
             $vraagRepository = new VraagRepository();
             $this->vragen = $vraagRepository->findBy(array(
                 VraagRepository::COLUMN_DOELSTELLING => $this->id
             ));
        }
        return $this->vragen;

    public function getVragen() {
        if (is_null($this->vragen)) {
            $doelstellingPerVraagRepository = new DoelstellingPerVraagRepository();
            $doelstellingenPerVraag = $doelstellingPerVraagRepository->findBy(array(
                DoelstellingPerVraagRepository::COLUMN_DOELSTELLING => $this->getId()
            ));

            $this->vragen = array();

            /** @var DoelstellingPerVraag $doelstellingPerVraag */
            foreach ($doelstellingenPerVraag as $doelstellingPerVraag) {
                $this->vragen[] = $doelstellingPerVraag->getVraag();
            }
        }
        return $this->vragen;
    }

}