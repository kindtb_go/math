<?php
namespace Entity;

use Entity\Repository\GesteldeVraagRepository;
use Entity\Repository\LeerkrachtRepository;
use Entity\Repository\VakRepository;
use Entity\Repository\EvaluatieSoortRepository;

class Toets {

    private $id;
    private $datum;
    private $leerkracht;
    private $vak;
    private $omschrijving;
    private $titel;
    private $soortEvaluatie;
    private $gesteldeVragen = null;
    //id
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    //datum
    public function getDatum()
    {
        return $this->datum;
    }

    public function setDatum($datum)
    {
        $this->datum = $datum;
    }
    //leerkracht
    public function getLeerkracht()
    {
        $leerkrachtRepository = new LeerkrachtRepository();
        $leerkracht = $leerkrachtRepository->find($this->leerkracht);
        return $leerkracht;
    }


    public function setLeerkracht($leerkracht)
    {
        $this->leerkracht = $leerkracht;
    }

    //klas
    public function getVak()
    {
        $vakRepository = new VakRepository();
        $vak = $vakRepository->find($this->vak);
        return $vak;
    }

    public function setVak($vak)
    {
        $this->vak = $vak;
    }

    //omschrijving
    public function getOmschrijving()
    {
        return $this->omschrijving;
    }

    public function setOmschrijving($omschrijving)
    {
        $this->omschrijving = $omschrijving;
    }

    //titel
    public function getTitel()
    {
        return $this->titel;
    }

    public function setTitel($titel)
    {
        $this->titel = $titel;
    }

    //soortevaluatie
    public function getSoortEvaluatie()
    {
        $evaluatieSoortRepository = new EvaluatieSoortRepository();
        $evaluatieSoort = $evaluatieSoortRepository->find($this->soortEvaluatie);
        return $evaluatieSoort;
    }

    public function setSoortEvaluatie($soortEvaluatie)
    {
        $this->soortEvaluatie = $soortEvaluatie;
    }

    function getGesteldeVragen() {
        if (is_null($this->gesteldeVragen)) {
             $gesteldeVraagRepository = new GesteldeVraagRepository();
             $this->gesteldeVragen = $gesteldeVraagRepository->findBy(array(
                 GesteldeVraagRepository::COLUMN_GEVRAAGDOPWELKETOETS => $this->id
             ));
        }
        return $this->gesteldeVragen;
    }

} 