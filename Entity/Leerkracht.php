<?php
namespace Entity;

use Entity\Repository\ToetsRepository;
use Entity\Repository\VakRepository;

class Leerkracht {

    /** @var int */
    private $id;

    /** @var string */
    private $naam;

    /** @var string */
    private $voornaam;

    /** @var array */
    private $vakken = null;

    /** @var array */
    private $toetsen = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     
    /**
     * @return string
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * @param string $naam
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;
    }

    /**
     * @return string
     */
    public function getVoornaam()
    {
        return $this->voornaam;
    }

    /**
     * @param string $voornaam
     */
    public function setVoornaam($voornaam)
    {
        $this->voornaam = $voornaam;
    }

    public function getVakken() {
        if (is_null($this->vakken)) {
            $vakRepository = new VakRepository();
            $this->vakken = $vakRepository->findBy(array(
                VakRepository::COLUMN_LEERKRACHT => $this->id
            ));
        }
        return $this->vakken;
    }
    /*-------------------*/
    public function getToetsen() {
        if (is_null($this->toetsen)) {
            $toetsRepository = new ToetsRepository();
            $this->toetsen = $toetsRepository->findBy(array(
                ToetsRepository::COLUMN_LEERKRACHT => $this->id
            ));
        }
        return $this->toetsen;
    }

}