<?php
namespace Entity;

use Entity\Repository\KlasRepository;
use Entity\Repository\LeerplanRepository;
use Entity\Repository\ToetsRepository;
use Entity\Repository\LeerkrachtRepository;
use Entity\Repository\SchooljaarRepository;

class Vak{

    /** @var int */
    private $id;

    /** @var string */
    private $naam;

    private $klas;

    private $leerplan;

    private $leerkracht;
    
    private $schooljaar;
    
    private $toetsen = null;

    function getToetsen() {
        if (is_null($this->toetsen)) {
             $toetsRepository = new ToetsRepository();
             $this->toetsen = $toetsRepository->findBy(array(
                 ToetsRepository::COLUMN_VAK => $this->id
             ));
        }
        return $this->toetsen;
    }

 

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * @param string $naam
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;
    }

    /**
     * @return mixed
     */
    public function getKlas()
    {
        $klasRepository = new KlasRepository();
        $klas = $klasRepository->find($this->klas);
        return $klas;
    }

    /**
     * @param mixed $klas
     */
    public function setKlas($klas)
    {
        $this->klas = $klas;
    }

    /**
     * @return mixed
     */
    public function getLeerplan()
    {
        $leerplanRepository = new LeerplanRepository();
        $leerplan = $leerplanRepository->find($this->leerplan);
        return $leerplan;
    }

    /**
     * @param mixed $leerplan
     */
    public function setLeerplan($leerplan)
    {
        $this->leerplan = $leerplan;
    }

    public function getLeerkracht() {
        $leerkrachtRepository = new LeerkrachtRepository();
        $leerkracht = $leerkrachtRepository->find($this->leerkracht);
        return $leerkracht;
    }

    public function setLeerkracht($leerkracht) {
        $this->leerkracht = $leerkracht;
    }
    
    public function getSchooljaar()
    {
        $schooljaarRepository = new SchooljaarRepository();
        $schooljaar = $schooljaarRepository->find($this->schooljaar);
        return $schooljaar;
    }

    public function setSchooljaar($schooljaar)
    {
        $this->schooljaar = $schooljaar;
    }
}