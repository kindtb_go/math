<?php
namespace Entity;

use Entity\Repository\VakRepository;

class Klas{

    /** @var int */
    private $id;

    /** @var string */
    private $klas;

    /** @var array */
    private $vakken = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     
    /**
     * @return string
     */
    public function getKlas()
    {
        return $this->klas;
    }

    /**
     * @param string $klas
     */
    public function setKlas($klas)
    {
        $this->klas = $klas;
    }

    public function getVakken() {
        if (is_null($this->vakken)) {
            $vakRepository = new VakRepository();
            $this->vakken = $vakRepository->findBy(array(
                VakRepository::COLUMN_KLAS => $this->getId()
            ));
        }
        return $this->vakken;
    }


}